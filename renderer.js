"use strict";
window.electron = require('electron')
const currentWindow = electron.remote.getCurrentWindow()
const session = currentWindow.webContents.session
const url = require('url')
const path = require('path')

// window.electron.ipcRenderer.domain = 'facebook.com'
// const BrowserWindow = electron.BrowserWindow
const BrowserWindow = require('electron').remote.BrowserWindow;

var db;
// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var fs = require('fs');
var globO = {
	userId : 100000707901459,
	friends : {},
	pages : {}
}
window.$ = window.jQuery = require('./jquery-3.2.1.min.js')
require('./jquery.waypoints.min.js')


String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) {
        return hash;
    }
    for (var i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
String.prototype.has = function(a){return this.indexOf(a) > -1}

Object.defineProperty(Array.prototype, 'has', {
  value: String.prototype.has ,
  itérable: false,
  writable: false
});


//rendre les object itérable via "for of"
Object.prototype[Symbol.iterator] = function*() {
    for(let key of Object.keys(this)) {
         yield this[key] 
    } 
}


var my = window.bortocial = {
	// on appelle la popup de login car il y a un 
	// truc dynamique dedans qui fait qu'on ne peut pas utiliser
	// le même formulaire tout le temps
	// on va empêcher le chargement de la page suivante et fermer la popup
	login(){
		// webview.preload = "./preload.js";
		var w = new BrowserWindow({width: 800, height: 600}).loadURL(url.format({
		    pathname: path.join(__dirname, 'form.html'),
		    protocol: 'file:',
		    slashes: true
		}))
		// var w = window.open('file:///home/borto/Bureau/')
		// w.webContents.executeJavaScript(injectedJs)
		// mainWindow.loadURL("data:text/html;charset=utf-8," + encodeURI(html));
		
		// document.body.appendChild(webview)

	},
	ajax: function ajax(url,o){
		var oReq = new XMLHttpRequest();
		if(o.postData){
			//oReq.open("loadFriends()POST", url, true);
			oReq.open("POST", url, true);
			oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		}else{
			oReq.open("GET", url, true);
		}
		oReq.onload = o.success;
		oReq.send(o.postData);
	},
	showPage(i){
		my.arr_pageGrp.forEach(function($pageGrp,j){
			if(j == i){
				my.$firstPageGroupVisible = $pageGrp;
				$pageGrp.show()
			}else{
				$pageGrp.hide()
			}
		})
	},
	addArticle : function addArticle($article){
		var art = $article;
		var h = art.dataset.store//.hashCode()
		if(!h){
			art = $($article).find('*[data-store]')[0]
			if(!art){
				console.error('no_Id_Found');
				return;
			}
			h = art.dataset.store;
		}
		try{
			var json = JSON.parse(h)
			delete json.previous_cursor
			delete json.linkdata
			art.dataset.store = h = JSON.stringify(json)
		}catch(e){
			console.error('not json')
		}
		var old = db.articles[h];
		var txt = $article.outerHTML
		if(old){
			return;
			// if( ~old.indexOf(txt) ) return;
			// db.articles[h].push(txt)
		}else{
			db.hashOrder.push(h)
			db.articles[h] = [txt]
		}
		my.saveData()
	},
	saveData : function(){
		var json = JSON.stringify(db)
		fs.writeFileSync('article.json',json, 'utf-8');
	},
	loadData : function(){
		try{
			db = require('./article.json')
		}catch(e){
			db = {}
		}
		db.articles = db.articles || {}
		db.hashOrder = db.hashOrder || []
	},
	init:function(){
		my.toShow = []
		my.labels = {}
		my.$pagination = $('#pagination')
		var $checkboxFilter = $('<div id="checkboxFilter" />').prependTo(document.body),
			autoReload = function(){
				setTimeout(() => {
					//my.loadMoreFeeds(0);
					autoReload()
				},200000)
				my.ajax('https://m.facebook.com/',{});
			},
			lastDisplayed = -1
		;
		my.loadData()
		my.loadFriends()
		my.displayArticles()
		// my.loadMoreFeeds(10000)
		autoReload()

		var $styleScr = $('<style id="scrollSt" />').appendTo(document.head)
		// var $style = $('<style id="st2">.page-group{display:none} .page-group:nth-child(1){display:block;} </style>').appendTo(document.head)
		var start = 0;
		var end = 300;
		var $feed = $('#feeds_parent').eq(0)
		// var $stories = $('.story_body_container')
		var now = 0;
		session.cookies.get({ url : 'https://facebook.com' }, function(error, cookies) {
		    console.log(cookies);
		    let cookieStr = ''
		    for (var i = 0; i < cookies.length; i++) {
		        let info = cookies[i];
		        cookieStr += `${info.name}=${info.value};`;
		        console.log(info.value, info.name);
		    }
		    console.log(cookieStr);
		});
		var d_feed = $feed[0];
		$feed.on('scroll', function(e){
			var scrollTop = d_feed.scrollTop,
				scrollBottom = d_feed.scrollHeight - scrollTop - d_feed.clientHeight,
				limit = 5000
			;
			if(scrollBottom < limit){
				var $next = my.$firstPageGroupVisible.next()
				if($next[0]){
					my.$firstPageGroupVisible = $next.show()
					my.$firstPageGroupVisible.prev().show().prev().hide().prev().hide()
				}
			}else if(scrollTop < limit){
				var $prev = my.$firstPageGroupVisible.prev();
				if($prev[0]){
					my.$firstPageGroupVisible = $prev;	
					my.$firstPageGroupVisible.show().next().show().next().hide().next().hide()
				}
			}

		})
	},
	showHideLabel: (function(){
		var $showHideLabelStyle = $('<style/>').appendTo(document.head)
		var toHideOld = []
		return function(){
			var bitmask = 0,$feed = $('#feeds_parent').eq(0)

			//on crée un bitmask pour faire plusieurs condition d'un coup
			for(var $lab of my.labels){
				var $chk = $lab.find('input')
				if($chk.prop('checked')){
					bitmask = bitmask | my.labelToInt[$chk[0].dataset.label]
				}
			}
			var j = 0;//element displayed
			$feed.hide()
			var arr_pageGrp = [];
			$feed[0].innerHTML = ''
			for(var art of my.toShow){
				if(!bitmask || (art[0].dataset.bitmask & bitmask) ){
					if(j%20 == 0){
						var iPage = j/20 |0;
						var $pageGrp = $('<div class="page-group" data-page="'+iPage+'" style="display:none"></div>');
						$feed.append($pageGrp)
						if(!j){
							var $firstPageGroupVisible = $pageGrp
						}
						arr_pageGrp[iPage] = $pageGrp;
					}
					$pageGrp.append(art);
					art.attr('j',j)
					++j
				}
				//$feed.html(arr_pageGrp)//tooslow
			}
			if($firstPageGroupVisible[0]){
				var $next = $firstPageGroupVisible;
				for(var i = 0;$next[0] && i < 3; ++i){
					$next = $next.show().next()
				}
			}
			$feed.show()

			var pages = ''
			arr_pageGrp.forEach(function(p,i){
				pages += '<span createFrom="bortosocial.showHideLabel" class="page-button" onclick="bortocial.showPage('+i+')">'+i+'</span>'
			})
			my.$pagination[0].innerHTML = pages;
			my.arr_pageGrp = arr_pageGrp;
			my.$firstPageGroupVisible = $firstPageGroupVisible
		}
	})(),
	loadMoreFeeds: (function(){
		var cursor, url = "https://m.facebook.com/stories.php";
		return function loadMoreFeeds(rec){
			console.log(url);
			my.ajax(url,{
				//postData : postData,
				success: function(resp){
					var html =  resp.currentTarget.responseText
					url =  "https://m.facebook.com/" + html.match(/stories.php\?aftercursor=.*?\"/g)[0]
					html = html.replace(/on(.{1,20}=)/g, 'no$1')//pas de listener ni mouseenter ... de fb.com
					var news = $('<html><div>'+html+'</div></html>');
						news = news.find('#m-top-of-feed').next()[0]
					$.each(news.children,function(i,article){
						$(article).find('script').remove()
						my.addArticle(article)
					})
					if(rec > 0){
						my.loadMoreFeeds(rec - 1)
					}else{
						my.displayArticles()
					}
				}
			})
		}
	})(),
	hidePostNotFriends: function hidePostNotFriends(){
		$('a[data-hovercard]').each(function(){
			var $feed = $(this).closest('[data-ftr]')
			var isFound = $feed.find('h5  a.profileLink').length
			if(!isFound){
				$feed.css('margin-left','-100px')
			}
		})
		$('a[aria-describedby]').css('background','red')
		$('a[data-hovercard]').css('background','orange')
		$('a.profileLink').css('background','green')
	},
	loadFriends: function loadFriends(){
		function decode(html){
			return JSON.parse(html.replace('for (;;);',''))
		}
		var url = 'https://www.facebook.com/ajax/typeahead/first_degree.php?viewer='+globO.userId+'&options[0]=friends_only&__a=1'
		my.ajax(url,{
			success: (x) => {
				var json = decode(x.currentTarget.responseText)
				json.payload.entries.forEach(function(o){
					if(o.type == 'user'){
						globO.friends[o.uid] = {
							firstname: o.firstname,
							lastname: o.lastname
						}
					}else if(o.type=='page'){
						globO.pages[o.uid] = {
							alias: o.alias
						}
					}
					globO.isFriendsAndPagesLoaded = true;
					fs.writeFileSync('paramsFb.txt', JSON.stringify(globO), 'utf-8');
				})
			}
		})
	},
	customAppearance($el){
		var $like = $($el[0].querySelector('footer a'))
		$like.after(
			`<span class="reaction haha" onclick="bortocial.reaction(4)">haha</span>
			<span class="reaction like" onclick="bortocial.reaction(2)">like</span>
			<span class="reaction love" onclick="bortocial.reaction(2)">love</span>
			<span class="reaction wow" onclick="bortocial.reaction(3)">wow</span>
			<span class="reaction sad" onclick="bortocial.reaction(7)">sad</span>
			<span class="reaction remove" onclick="bortocial.reaction(0)">remove</span>`
		)
	},
	displayArticles : (function(){
		var newLabels,
			lastDisplayed = 0,
			classifiers = [
				function(o){
					if(o.$el[0].innerText.has('BBC')){
						o.addLabel(o.$el,'BBC')
					}
					if(o.$el[0].innerText.has('Ottawa')){
						o.addLabel(o.$el,'Québec')
					}
					if(o.$el[0].innerText.has('Livia') || o.$el[0].innerText.has('livia')){
						o.addLabel(o.$el,'Livia')
					}
				}
			],
			iNextLabel = -1,//we use a binary number 0b0000000000 where 0 is the right
			$feed = $ ('#feeds_parent').eq(0),
			classifierUtil = {
				addLabel: function($el,name){
					newLabels[name] = 1;
					if(!(my.labelToInt[name] > -1)){
						my.labelToInt[name] = Math.pow(2,++iNextLabel);
					}
					$el[0].dataset.bitmask = $el[0].dataset.bitmask | my.labelToInt[name]
				}
			},
			checkboxFilter = $('<div id="checkboxFilter" style="z-index:2;position:fixed;top:0;background:grey"/>').prependTo(document.body)

		;
		if(!$feed.length){
			$feed = $('<div id="feeds_parent" data-sigil="m-feed-story-attachments-element" data-autoid="autoid_10">')
			$('#firstDiv').after($feed);
		}

		return function(){
			// if(++nbCall < 2) return;
			newLabels = {}
			// my.articleByLabel = my.articleByLabel || {}

			my.labelToInt = my.labelToInt || {}
			for(var i = lastDisplayed; i < db.hashOrder.length; ++i){
				var hash = db.hashOrder[i];
				for(var html of db.articles[hash]){
					var $el = $(html)
					my.customAppearance($el);
					var article = $el[0]
					classifiers.forEach(function(classifier){
						classifierUtil.$el = $el;
						classifier(classifierUtil)
						my.toShow.push($el);
					})
					$.each(globO.friends,function(key,obj){
						if(~article.innerHTML.indexOf(key)){
							article.style.border = "solid red 2px";
						}
					})
					$.each(globO.pages,function(key,obj){
						if(~article.innerHTML.indexOf(key)){
							article.style.border = "solid blue 2px";
						}
					})
				}
			}
			lastDisplayed = i;
			my.hidePostNotFriends();
			$.each(newLabels,function(lab){
				if(!my.labels[lab]){
					my.labels[lab] = $('<label><input data-label="'+lab+'" type="checkbox" onchange="bortocial.showHideLabel()">'+lab+'</label>')
					checkboxFilter.append(my.labels[lab])
				}
			})
			my.showHideLabel()

		}
	})()
}




my.init()





